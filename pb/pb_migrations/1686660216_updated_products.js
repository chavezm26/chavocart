migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("h1hrl8shvniatuy")

  // remove
  collection.schema.removeField("4nf7gznh")

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("h1hrl8shvniatuy")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "4nf7gznh",
    "name": "productCod",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  return dao.saveCollection(collection)
})
