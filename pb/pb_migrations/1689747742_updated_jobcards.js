migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("0ehmos6w3lguca4")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "fyphirzy",
    "name": "products",
    "type": "json",
    "required": false,
    "unique": false,
    "options": {}
  }))

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "xkdl47s0",
    "name": "quantity",
    "type": "json",
    "required": false,
    "unique": false,
    "options": {}
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("0ehmos6w3lguca4")

  // remove
  collection.schema.removeField("fyphirzy")

  // remove
  collection.schema.removeField("xkdl47s0")

  return dao.saveCollection(collection)
})
