migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("h1hrl8shvniatuy")

  collection.listRule = ""

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("h1hrl8shvniatuy")

  collection.listRule = null

  return dao.saveCollection(collection)
})
