migrate((db) => {
  const collection = new Collection({
    "id": "0ehmos6w3lguca4",
    "created": "2023-06-20 07:23:05.584Z",
    "updated": "2023-06-20 07:23:05.584Z",
    "name": "jobcards",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "lhurc0w8",
        "name": "supplierName",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      },
      {
        "system": false,
        "id": "9wkkjxit",
        "name": "purchaseOrder",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      },
      {
        "system": false,
        "id": "1k6m1cvb",
        "name": "contactName",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      },
      {
        "system": false,
        "id": "9calz3dd",
        "name": "email",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      },
      {
        "system": false,
        "id": "p5fj6a5f",
        "name": "phoneNumber",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      }
    ],
    "indexes": [],
    "listRule": null,
    "viewRule": null,
    "createRule": null,
    "updateRule": null,
    "deleteRule": null,
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("0ehmos6w3lguca4");

  return dao.deleteCollection(collection);
})
