migrate((db) => {
  const collection = new Collection({
    "id": "ubvvobgln1jdlkv",
    "created": "2023-08-21 08:31:43.300Z",
    "updated": "2023-08-21 08:31:43.300Z",
    "name": "customers",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "hjrdp4j0",
        "name": "customerName",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      },
      {
        "system": false,
        "id": "bvb3hxvu",
        "name": "debtorCode",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      }
    ],
    "indexes": [],
    "listRule": null,
    "viewRule": null,
    "createRule": null,
    "updateRule": null,
    "deleteRule": null,
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("ubvvobgln1jdlkv");

  return dao.deleteCollection(collection);
})
