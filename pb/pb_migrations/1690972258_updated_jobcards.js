migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("0ehmos6w3lguca4")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "xc4tk9pp",
    "name": "productCode",
    "type": "json",
    "required": false,
    "unique": false,
    "options": {}
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("0ehmos6w3lguca4")

  // remove
  collection.schema.removeField("xc4tk9pp")

  return dao.saveCollection(collection)
})
