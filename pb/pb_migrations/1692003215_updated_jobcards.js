migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("0ehmos6w3lguca4")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "sn9gdluz",
    "name": "customerName",
    "type": "text",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "pattern": ""
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("0ehmos6w3lguca4")

  // remove
  collection.schema.removeField("sn9gdluz")

  return dao.saveCollection(collection)
})
