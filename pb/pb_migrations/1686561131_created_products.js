migrate((db) => {
  const collection = new Collection({
    "id": "h1hrl8shvniatuy",
    "created": "2023-06-12 09:12:11.133Z",
    "updated": "2023-06-12 09:12:11.133Z",
    "name": "products",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "z3elafb5",
        "name": "productName",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      }
    ],
    "indexes": [],
    "listRule": null,
    "viewRule": null,
    "createRule": null,
    "updateRule": null,
    "deleteRule": null,
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("h1hrl8shvniatuy");

  return dao.deleteCollection(collection);
})
