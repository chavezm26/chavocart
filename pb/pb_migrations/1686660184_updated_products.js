migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("h1hrl8shvniatuy")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "89f4ilk2",
    "name": "productCode",
    "type": "text",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "pattern": ""
    }
  }))

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "4nf7gznh",
    "name": "productCod",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("h1hrl8shvniatuy")

  // remove
  collection.schema.removeField("89f4ilk2")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "4nf7gznh",
    "name": "productCode",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  return dao.saveCollection(collection)
})
