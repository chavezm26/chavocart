// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unused_import, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:jobcard/constants/app_constants.dart';

import 'package:jobcard/provider/pocket_base_provider.dart';
import 'package:jobcard/service/authentication_service.dart';

import 'package:jobcard/views/home_view.dart';
import 'package:provider/provider.dart';
import 'package:jobcard/navigation/router.dart' as route;

import 'injection/injection.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies();

  runApp(MyWidget());
}

class MyWidget extends StatelessWidget {
  MyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (_) => independentServices),
        Provider(create: (_) => dependentServices),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        onGenerateRoute: route.generateRoute,
        initialRoute: pb.authStore.isValid ? '/' : 'login',
      ),
    );
  }
}
