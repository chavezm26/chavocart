import 'package:flutter/material.dart';

// ignore: non_constant_identifier_names
PreferredSizeWidget Custom_AppBar(BuildContext context, String title) {
  return PreferredSize(
      preferredSize: const Size.fromHeight(50),
      child: AppBar(
          title: Center(child: Text(title)),
          actions: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pushNamed(context, 'cart');
              },
            )
          ],
          backgroundColor: Colors.black));
}
