// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:jobcard/constants/app_constants.dart';
import 'package:jobcard/navigation/nav_bar.dart';
import 'package:jobcard/views/login_view.dart';
import 'package:jobcard/views/new_jobcard_view.dart';
import 'package:jobcard/views/profile_view.dart';
import 'package:jobcard/views/register_view.dart';

import '../views/cart_view.dart';
import '../views/home_view.dart';
import '../views/jobcard_view.dart';
import '../views/products_view.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case RoutePaths.Nav:
      return MaterialPageRoute(builder: (context) => NavBar());
    case RoutePaths.Login:
      return MaterialPageRoute(builder: (context) => LoginView());
    case RoutePaths.Register:
      return MaterialPageRoute(builder: (context) => RegisterView());
    case RoutePaths.Profile:
      return MaterialPageRoute(builder: (context) => ProfileView());
    case RoutePaths.Home:
      return MaterialPageRoute(builder: (context) => HomeView());
    case RoutePaths.Products:
      return MaterialPageRoute(builder: (context) => ProductsView());
    case RoutePaths.Jobcard:
      return MaterialPageRoute(builder: (context) => JCView());
    case RoutePaths.Cart:
      return MaterialPageRoute(builder: (context) => CartView());
    case RoutePaths.NewJC:
      return MaterialPageRoute(builder: (context) => NewJobcard());

    default:
      return MaterialPageRoute(
          builder: (_) => Scaffold(
                body: Center(
                  child: Text('No route defined for ${settings.name}'),
                ),
              ));
  }
}
