// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:jobcard/navigation/nav_drawer.dart';
import 'package:jobcard/navigation/router.dart' as route;

import '../model/user_model.dart';

int _selectedIndex = 0;

class NavBar extends StatefulWidget {
  const NavBar({super.key});

  @override
  State<NavBar> createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  late User _currentUser;
  User get currentUser => _currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      body: Navigator(
        key: _navigatorKey,
        onGenerateRoute: route.generateRoute,
        initialRoute: 'home',
      ),
      bottomNavigationBar: GNav(
        backgroundColor: Colors.black,
        color: Colors.white,
        activeColor: Colors.grey,
        hoverColor: Colors.grey,
        tabs: const [
          GButton(
            icon: Icons.home,
            text: "Home",
          ),
          GButton(
            icon: Icons.account_circle,
            text: "Profile",
          ),
        ],
        selectedIndex: _selectedIndex,
        onTabChange: (index) {
          setState(() {
            getView(index);
          });
        },
      ),
    );
  }

  Object? getView(int index) {
    switch (index) {
      case 0:
        return _navigatorKey.currentState?.pushNamed('home');

      case 1:
        return _navigatorKey.currentState?.pushNamed('profile');

      default:
        _navigatorKey.currentState?.pushNamed('home');
    }
    return null;
  }
}
