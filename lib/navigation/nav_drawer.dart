import 'package:flutter/material.dart';
import '../injection/injection.dart';
import '../service/authentication_service.dart';
import '../view_model/login_view_model.dart';

class NavDrawer extends StatefulWidget {
  const NavDrawer({
    super.key,
  });

  @override
  State<NavDrawer> createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  final LoginModel auth =
      LoginModel(authenticationService: getIt<AuthenticationService>());
  final int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.black,
        child: ListView(children: [
          const SizedBox(
            height: 48,
          ),
          buildMenuItem(
              text: "Profile",
              icon: Icons.account_circle,
              onClicked: () => selectedItem(context, 0)),
          buildMenuItem(
              text: "Home",
              icon: Icons.home,
              onClicked: () => selectedItem(context, 1)),
          buildMenuItem(
              text: "Products",
              icon: Icons.home,
              onClicked: () => selectedItem(context, 2)),
          buildMenuItem(
              text: "Jobcard",
              icon: Icons.shopping_cart_checkout_outlined,
              onClicked: () => selectedItem(context, 3)),
          buildMenuItem(
              text: "Logout",
              icon: Icons.logout,
              onClicked: () => selectedItem(context, 4)),
        ]),
      ),
    );
  }

  Widget buildMenuItem(
      {required String text, required IconData icon, VoidCallback? onClicked}) {
    const color = Colors.white;

    return ListTile(
      leading: Icon(
        icon,
        color: color,
      ),
      title: Text(text, style: const TextStyle(color: color)),
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.pushNamed(context, 'profile');

        break;
      case 1:
        Navigator.pushNamed(context, 'home');
        break;
      case 2:
        Navigator.pushNamed(context, "products");
        break;
      case 3:
        Navigator.pushNamed(context, "jobcard");
        break;
      case 4:
        signOut();
        Navigator.pushNamed(context, "login");
        break;
    }
  }

  Future signOut() async {
    var success = await auth.signout();

    return success;
  }
}
