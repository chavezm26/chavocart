import 'package:flutter/material.dart';
import 'package:pocketbase/pocketbase.dart';
import 'package:provider/single_child_widget.dart';

List<SingleChildWidget> providers = [
  ...independentServices,
  ...dependentServices
];

List<SingleChildWidget> independentServices = [];
List<SingleChildWidget> dependentServices = [];

class PocketBaseProvider extends ChangeNotifier {
  final PocketBase _pb = PocketBase(
    const String.fromEnvironment('users',
        defaultValue: 'http://127.0.0.1:8090/'),
  );
}
