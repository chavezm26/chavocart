import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogBox extends StatefulWidget {
  const DialogBox({super.key});

  @override
  State<DialogBox> createState() => _DialogBoxState();

  void dialogBox(BuildContext context, String text, bool check) {
    String title = "";
    if (check) {
      title = "Success";
    } else {
      title = "Oops";
    }
    showDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(title),
            content: Text(text),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text("Close")),
            ],
          );
        });
  }
}

class _DialogBoxState extends State<DialogBox> {
  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
