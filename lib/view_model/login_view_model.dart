import "package:flutter/material.dart";
import "package:jobcard/service/authentication_service.dart";

import "../model/user_model.dart";

class LoginModel extends ChangeNotifier {
  final AuthenticationService _authenticationService;
  late User _currentUser;
  User get currentUser => _currentUser;

  LoginModel({
    required AuthenticationService authenticationService,
  }) : _authenticationService = authenticationService;

  Future login({required String email, required String password}) async {
    var success = await _authenticationService.loginWithEmail(
        email: email, password: password);

    return success;
  }

  Future register({
    required String username,
    required String email,
    required String password,
  }) async {
    var success = await _authenticationService.signUp(
        username: username, email: email, password: password);

    return success;
  }

  Future signout() async {
    var success = await _authenticationService.signOut();

    return success;
  }

  Future updateUser({
    required String username,
    required String email,
    required String passwordConfirm,
    required String passwordNew,
    required String oldPassword,
  }) async {
    var success = await _authenticationService.updateApi(
        username: username,
        email: email,
        passwordConfirm: passwordConfirm,
        passwordNew: passwordNew,
        oldPassword: oldPassword);

    return success;
  }
}
