import 'package:flutter/material.dart';
import 'package:jobcard/service/products_service.dart';

import '../model/products_model.dart';

class ProductsModel extends ChangeNotifier {
  final ProductService _productService;
  List<Products> _products = [];
  List<Products> get products => _products;

  ProductsModel({
    required ProductService productService,
  }) : _productService = productService;

  Future getProducts() async {
    var success = await _productService.fetchProducts();
    _products = success!;

    notifyListeners();
    return success;
  }

  Future searchProducts(String filter) async {
    var success = await _productService.searchProducts(filter);
    _products = success!;

    notifyListeners();
    return success;
  }

  Future newProduct({
    required String productName,
    required String productCode,
  }) async {
    var success = await _productService.newProduct(
        productName: productName, productCode: productCode);

    return success;
  }
}
