import 'package:flutter/material.dart';
import 'package:jobcard/model/jobcard_model.dart';
import 'package:jobcard/service/jobcard_service.dart';

class JCModel extends ChangeNotifier {
  final JobCardService _jobcardService;
  List<Cards> _cards = [];
  List<Cards> get cards => _cards;

  JCModel({
    required JobCardService jobCardService,
  }) : _jobcardService = jobCardService;

  Future newJobcard({
    required String supplierName,
    required String purchaseOrder,
    required String customerName,
    required String contactName,
    required String email,
    required String phoneNumber,
    required List products,
    required List productCode,
    required List quantity,
    required String jobcardNo,
  }) async {
    var success = await _jobcardService.newJC(
        supplierName: supplierName,
        purchaseOrder: purchaseOrder,
        contactName: contactName,
        email: email,
        phoneNumber: phoneNumber,
        products: products,
        productCode: productCode,
        quantity: quantity,
        jobcardNo: jobcardNo,
        customerName: customerName);
    notifyListeners();
    return success;
  }

  Future getJC() async {
    var success = await _jobcardService.fetchJC();

    _cards = success!;

    notifyListeners();
    return success;
  }

  Future updateJobcard({
    required String id,
    required String supplierName,
    required String customerName,
    required String purchaseOrder,
    required String contactName,
    required String email,
    required String phoneNumber,
    required List products,
    required List productCode,
    required List quantity,
  }) async {
    var success = await _jobcardService.updateJC(
      id: id,
      supplierName: supplierName,
      purchaseOrder: purchaseOrder,
      contactName: contactName,
      email: email,
      phoneNumber: phoneNumber,
      products: products,
      productCode: productCode,
      quantity: quantity,
      customerName: customerName,
    );
    notifyListeners();
    return success;
  }

  Future updateProducts({
    required String id,
    required List products,
    required List productCode,
    required List quantity,
  }) async {
    var success = await _jobcardService.updateProducts(
      id: id,
      products: products,
      productCode: productCode,
      quantity: quantity,
    );
    notifyListeners();
    return success;
  }

  Future itemsInCart({required String id}) async {
    var response = await _jobcardService.itemsInCart(id: id);

    notifyListeners();
    return response;
  }

  void removeItems() {
    _jobcardService.removeItems();
    notifyListeners();
  }

  void clearForm() {
    _jobcardService.clearForm();
    notifyListeners();
  }
}
