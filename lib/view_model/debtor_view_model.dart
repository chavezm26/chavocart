import 'package:flutter/material.dart';

import '../model/debtor_model.dart';
import '../service/debtor_service.dart';

class DebtorModel extends ChangeNotifier {
  final DebtorService _debtorService;

  List<Debtor> _debtor = [];
  List<Debtor> get debtor => _debtor;

  DebtorModel({
    required DebtorService debtorService,
  }) : _debtorService = debtorService;

  Future searchDebtor(String filter) async {
    var success = await _debtorService.searchDebtor(filter);
    _debtor = success!;

    notifyListeners();
    return success;
  }
}
