// ignore_for_file: prefer_const_constructors, use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:jobcard/navigation/app_bar.dart';

import '../navigation/nav_drawer.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavDrawer(),
        appBar: Custom_AppBar(context, 'Home'),
        backgroundColor: Colors.grey,
        body: Center(
          child: Column(children: const <Widget>[
            SizedBox(height: 20),
            Text(
              "This is the home screen",
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ]),
        ));
  }
}
