// ignore_for_file: use_build_context_synchronously, prefer_const_constructors, must_be_immutable

import 'package:flutter/material.dart';
import 'package:jobcard/dialog_service/basic_dialog.dart';
import 'package:jobcard/service/authentication_service.dart';
import 'package:jobcard/view_model/login_view_model.dart';
import 'package:jobcard/ui/register_screen.dart';
import 'package:provider/provider.dart';

import '../injection/injection.dart';

class RegisterView extends StatelessWidget {
  RegisterView({super.key});

  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();

  final TextEditingController _usernameController = TextEditingController();

  List isValid = [];

  final DialogBox dialog = DialogBox();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LoginModel>.value(
      value: LoginModel(authenticationService: getIt<AuthenticationService>()),
      child: Consumer<LoginModel>(
        builder: (context, model, child) => Scaffold(
            appBar: AppBar(
                automaticallyImplyLeading: false,
                title: Center(child: Text('Register')),
                backgroundColor: Colors.black),
            backgroundColor: Colors.grey,
            body: Center(
              child: Container(
                height: 800,
                width: 800,
                child: Column(
                  children: <Widget>[
                    RegisterHeader(
                        emailController: _emailController,
                        passwordController: _passwordController,
                        usernameController: _usernameController),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ElevatedButton(
                          style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                              backgroundColor: Colors.black),
                          child: Text("Register"),
                          onPressed: () async {
                            try {
                              isValid = await model.register(
                                  username: _usernameController.text
                                      .trim()
                                      .toLowerCase(),
                                  email: _emailController.text
                                      .trim()
                                      .toLowerCase(),
                                  password: _passwordController.text
                                      .trim()
                                      .toLowerCase());

                              if (isValid[0]) {
                                await model.login(
                                    email: _emailController.text
                                        .trim()
                                        .toLowerCase(),
                                    password: _passwordController.text
                                        .trim()
                                        .toLowerCase());
                                dialog.dialogBox(context,
                                    "New User has been Registered", isValid[0]);

                                Navigator.pushNamed(context, "/");
                              }
                              if (!isValid[0]) {
                                dialog.dialogBox(
                                    context, isValid[1], isValid[0]);
                              }
                            } catch (e) {
                              dialog.dialogBox(context, isValid[1], isValid[0]);
                            }
                          },
                        ),
                        ElevatedButton(
                          style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                              backgroundColor: Colors.black),
                          child: Text("Back"),
                          onPressed: () {
                            Navigator.pushNamed(context, "login");
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )),
      ),
    );
  }
}
