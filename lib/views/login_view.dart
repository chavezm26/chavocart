// ignore_for_file: prefer_const_constructors, curly_braces_in_flow_control_structures, use_build_context_synchronously, unused_import, unnecessary_import, implementation_imports, must_be_immutable

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:jobcard/dialog_service/basic_dialog.dart';
import 'package:jobcard/injection/injection.dart';
import 'package:jobcard/service/authentication_service.dart';

import 'package:jobcard/view_model/login_view_model.dart';
import 'package:jobcard/ui/login_screen.dart';
import 'package:provider/provider.dart';
import 'package:email_validator/email_validator.dart';

class LoginView extends StatelessWidget {
  LoginView({super.key});

  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();

  final AuthenticationService authenticationService = AuthenticationService();

  List isValid = [];

  final DialogBox dialog = DialogBox();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LoginModel>.value(
      value: LoginModel(authenticationService: getIt<AuthenticationService>()),
      child: Consumer<LoginModel>(
        builder: (context, model, child) => Scaffold(
            appBar: AppBar(
                automaticallyImplyLeading: false,
                title: Center(child: Text('Login')),
                backgroundColor: Colors.black),
            backgroundColor: Colors.grey,
            body: Center(
              child: Container(
                height: 800,
                width: 800,
                child: Column(
                  children: <Widget>[
                    LoginHeader(
                        emailController: _emailController,
                        passwordController: _passwordController),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ElevatedButton(
                          style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                              backgroundColor: Colors.black),
                          child: Text("Login"),
                          onPressed: () async {
                            try {
                              isValid = await model.login(
                                  email: _emailController.text
                                      .trim()
                                      .toLowerCase(),
                                  password: _passwordController.text
                                      .trim()
                                      .toLowerCase());

                              if (isValid[0]) {
                                Navigator.pushNamed(context, "/");
                              }
                              if (!isValid[0]) {
                                dialog.dialogBox(
                                    context, isValid[1], isValid[0]);
                              }
                            } catch (e) {
                              dialog.dialogBox(context, isValid[1], isValid[0]);
                            }
                          },
                        ),
                        ElevatedButton(
                          style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                              backgroundColor: Colors.black),
                          child: Text("Register"),
                          onPressed: () {
                            Navigator.pushNamed(context, "register");
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )),
      ),
    );
  }
}
