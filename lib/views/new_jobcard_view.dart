// ignore_for_file: use_build_context_synchronously, must_be_immutable, prefer_typing_uninitialized_variables

import 'dart:math';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:jobcard/model/cartItems_model.dart';
import 'package:jobcard/model/debtor_model.dart';
import 'package:jobcard/model/jobcard_model.dart';
import 'package:jobcard/service/cart_service.dart';
import 'package:jobcard/service/debtor_service.dart';
import 'package:jobcard/view_model/debtor_view_model.dart';
import 'package:provider/provider.dart';

import '../dialog_service/basic_dialog.dart';
import '../injection/injection.dart';
import '../model/products_model.dart';

import '../service/jobcard_service.dart';
import '../service/products_service.dart';
import '../ui/forms/jobcard_form.dart';

import '../view_model/jobcard_view_model.dart';
import '../view_model/product_view_model.dart';

class NewJobcard extends StatefulWidget {
  const NewJobcard({super.key});

  @override
  State<NewJobcard> createState() => _NewJobcardState();
}

class _NewJobcardState extends State<NewJobcard> {
  Cards cards = getIt<JobCardService>().cards;
  Debtor debtor = getIt<JobCardService>().debtor;
  List items = getIt<CartService>().items;

  List code = getIt<CartService>().code;

  List quant = getIt<CartService>().quant;

  final ProductsModel productsModel =
      ProductsModel(productService: getIt<ProductService>());

  final DebtorModel debtorModel =
      DebtorModel(debtorService: getIt<DebtorService>());

  final DialogBox dialog = const DialogBox();

  final CartService cart = CartService();

  bool result = false;

  var itemName;
  var itemCode;

  var customerName;
  var debtorCode;

  var jcNumber;
  var listLength;
  CartItems products = getIt<JobCardService>().cartItems;

  final JCModel jcModel = JCModel(jobCardService: getIt<JobCardService>());
  final snackBar =
      const SnackBar(content: Text("New Jobcard has been created"));
  List isValid = [];

  int random(int min, int max) {
    return min + Random().nextInt(max - min);
  }

  @override
  void dispose() {
    if (products.productCode.isNotEmpty) {
      jcModel.removeItems();
      cart.removeAllFromCart();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final TextEditingController supplierNameController =
        TextEditingController(text: cards.supplierName);
    final TextEditingController purchaseOrderController =
        TextEditingController(text: cards.purchaseOrder);
    final TextEditingController contactNameController =
        TextEditingController(text: cards.contactName);
    final TextEditingController emailController =
        TextEditingController(text: cards.email);
    final TextEditingController phoneNumberController =
        TextEditingController(text: cards.phoneNumber);
    final TextEditingController quantityController = TextEditingController();

    return ChangeNotifierProvider<JCModel>.value(
      value: JCModel(jobCardService: getIt<JobCardService>()),
      child: Consumer<JCModel>(
        builder: (context, model, child) => Scaffold(
          appBar: AppBar(
              automaticallyImplyLeading: true,
              title: const Center(child: Text('New JobCard')),
              actions: <Widget>[
                IconButton(
                  icon: const Icon(
                    Icons.shopping_cart,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, 'cart');
                  },
                )
              ],
              backgroundColor: Colors.black),
          backgroundColor: Colors.grey,
          body: Center(
            child: Container(
              height: 800,
              width: 800,
              child: Column(
                children: <Widget>[
                  Form(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Column(
                      children: <Widget>[
                        JCForm(
                            controller: supplierNameController,
                            label: "Supplier Name",
                            icon: const Icon(Icons.business)),
                        DropdownSearch<Debtor>(
                          dropdownDecoratorProps: const DropDownDecoratorProps(
                              dropdownSearchDecoration:
                                  InputDecoration(labelText: "Customer")),
                          asyncItems: (String filter) async {
                            if (filter.isEmpty) {
                              var response =
                                  await debtorModel.searchDebtor("Null");

                              return response;
                            } else {
                              var response =
                                  await debtorModel.searchDebtor(filter);

                              return response;
                            }
                          },
                          itemAsString: (Debtor u) => u.userAsString(),
                          onChanged: (Debtor? data) {
                            customerName = data?.customerName;
                            debtorCode = data?.debtorCode;
                          },
                          selectedItem: debtor,
                          popupProps:
                              const PopupPropsMultiSelection.modalBottomSheet(
                            isFilterOnline: true,
                            showSelectedItems: false,
                            showSearchBox: true,
                          ),
                        ),
                        TextFormField(
                            controller: purchaseOrderController,
                            decoration: const InputDecoration(
                                labelText: "Purchase Order Number",
                                icon: Icon(Icons.numbers)),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Field must not be empty";
                              } else if (int.tryParse(value) == null) {
                                return 'Only Number are allowed';
                              }
                              return null;
                            }),
                        JCForm(
                            controller: contactNameController,
                            label: "Contact Name",
                            icon: const Icon(Icons.person)),
                        TextFormField(
                            controller: emailController,
                            decoration: const InputDecoration(
                                labelText: "Email Address",
                                icon: Icon(Icons.mail)),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Field must not be empty";
                              } else if (!EmailValidator.validate(value)) {
                                return "Email address is invalid";
                              }
                              return null;
                            }),
                        TextFormField(
                            controller: phoneNumberController,
                            decoration: const InputDecoration(
                                labelText: "Phone Numer",
                                icon: Icon(Icons.phone)),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Field must not be empty";
                              } else if (int.tryParse(value) == null) {
                                return 'Only Number are allowed';
                              } else if (value.length != 10) {
                                return 'Invalid Phone Number';
                              }
                              return null;
                            }),
                        DropdownSearch<Products>(
                          dropdownDecoratorProps: const DropDownDecoratorProps(
                              dropdownSearchDecoration:
                                  InputDecoration(labelText: "Products")),
                          asyncItems: (String filter) async {
                            if (filter.isEmpty) {
                              var response =
                                  await productsModel.searchProducts("Null");
                              return response;
                            } else {
                              var response =
                                  await productsModel.searchProducts(filter);

                              return response;
                            }
                          },
                          itemAsString: (Products u) => u.userAsString(),
                          onChanged: (Products? data) {
                            itemName = data?.productName;
                            itemCode = data?.productCode;
                          },
                          selectedItem: null,
                          popupProps:
                              const PopupPropsMultiSelection.modalBottomSheet(
                            isFilterOnline: true,
                            showSelectedItems: false,
                            showSearchBox: true,
                          ),
                        ),
                        JCForm(
                            controller: quantityController,
                            label: "Quantity",
                            icon: const Icon(Icons.numbers))
                      ],
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ElevatedButton(
                          style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                              backgroundColor: Colors.black),
                          child: const Text("Add product to cart"),
                          onPressed: () {
                            if (itemName != null) {
                              try {
                                result = cart.addToCart(itemName, itemCode,
                                    quantityController.text);
                                dialog.dialogBox(context,
                                    "Item has been added to the cart", true);
                                quantityController.clear();

                                itemName = null;
                              } catch (e) {
                                dialog.dialogBox(context,
                                    "Item Already added to cart", false);
                              }
                            }
                          }),
                      ElevatedButton(
                          style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                              backgroundColor: Colors.black),
                          child: const Text("Submit"),
                          onPressed: () async {
                            await jcModel.getJC();

                            if (supplierNameController.text.isNotEmpty ||
                                purchaseOrderController.text.isNotEmpty ||
                                contactNameController.text.isNotEmpty ||
                                emailController.text.isNotEmpty ||
                                phoneNumberController.text.isNotEmpty) {
                              if (cards.id.isEmpty) {
                                try {
                                  listLength = jcModel.cards.length;

                                  jcNumber = int.parse(
                                      jcModel.cards[listLength - 1].jobcardNo);
                                  jcNumber++;

                                  isValid = await model.newJobcard(
                                      supplierName:
                                          supplierNameController.text.trim(),
                                      purchaseOrder:
                                          purchaseOrderController.text.trim(),
                                      contactName:
                                          contactNameController.text.trim(),
                                      email: emailController.text.trim(),
                                      phoneNumber:
                                          phoneNumberController.text.trim(),
                                      products: items.toList(),
                                      productCode: code.toList(),
                                      quantity: quant.toList(),
                                      jobcardNo: jcNumber.toString(),
                                      customerName: customerName);

                                  if (isValid[0]) {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                    supplierNameController.clear();
                                    purchaseOrderController.clear();
                                    contactNameController.clear();
                                    emailController.clear();
                                    phoneNumberController.clear();

                                    cart.removeAllFromCart();
                                    Navigator.pushNamed(context, 'jobcard');
                                  } else if (!isValid[0]) {
                                    dialog.dialogBox(
                                        context, isValid[1], isValid[0]);
                                  }
                                } catch (e) {
                                  dialog.dialogBox(
                                      context, isValid[1], isValid[0]);
                                }
                              } else {
                                try {
                                  isValid = await model.updateJobcard(
                                    id: cards.id,
                                    supplierName:
                                        supplierNameController.text.trim(),
                                    purchaseOrder:
                                        purchaseOrderController.text.trim(),
                                    contactName:
                                        contactNameController.text.trim(),
                                    email: emailController.text.trim(),
                                    phoneNumber:
                                        phoneNumberController.text.trim(),
                                    products: items.toList(),
                                    productCode: code.toList(),
                                    quantity: quant.toList(),
                                    customerName: customerName,
                                  );

                                  if (isValid[0]) {
                                    dialog.dialogBox(context,
                                        "Jobcard has been updated", isValid[0]);
                                    supplierNameController.clear();
                                    purchaseOrderController.clear();
                                    contactNameController.clear();
                                    emailController.clear();
                                    phoneNumberController.clear();

                                    cart.removeAllFromCart();
                                  } else if (!isValid[0]) {
                                    dialog.dialogBox(
                                        context, isValid[1], isValid[0]);
                                  }
                                } catch (e) {
                                  dialog.dialogBox(
                                      context, isValid[1], isValid[0]);
                                }
                              }
                            } else {
                              dialog.dialogBox(context,
                                  "One or more fields are empty", false);
                            }
                          }),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
