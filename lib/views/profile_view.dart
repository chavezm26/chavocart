// ignore_for_file: use_build_context_synchronously, prefer_const_constructors, must_be_immutable

import 'package:flutter/material.dart';
import 'package:jobcard/dialog_service/basic_dialog.dart';
import 'package:jobcard/navigation/app_bar.dart';
import 'package:jobcard/service/authentication_service.dart';
import 'package:jobcard/navigation/nav_drawer.dart';
import 'package:jobcard/view_model/login_view_model.dart';
import 'package:jobcard/ui/profile_screen.dart';
import 'package:provider/provider.dart';

import '../injection/injection.dart';
import '../model/user_model.dart';

class ProfileView extends StatelessWidget {
  ProfileView({super.key});
  final User user = getIt<AuthenticationService>().currentUser;
  final snackBar = const SnackBar(content: Text("Profile has been updated"));
  List isValid = [];

  final DialogBox dialog = DialogBox();

  @override
  Widget build(BuildContext context) {
    final TextEditingController emailController =
        TextEditingController(text: user.email);
    final TextEditingController usernameController =
        TextEditingController(text: user.name);
    final TextEditingController passwordNewController =
        TextEditingController(text: user.password);
    final TextEditingController passwordConfirmController =
        TextEditingController(text: user.confirmPassword);

    return ChangeNotifierProvider<LoginModel>.value(
        value:
            LoginModel(authenticationService: getIt<AuthenticationService>()),
        child: Consumer<LoginModel>(
            builder: (context, model, child) => Scaffold(
                drawer: NavDrawer(),
                appBar: Custom_AppBar(context, 'Profile'),
                backgroundColor: Colors.grey,
                body: Center(
                  child: Container(
                    height: 800,
                    width: 800,
                    child: Column(
                      children: <Widget>[
                        ProfileHeader(
                          emailController: emailController,
                          usernameController: usernameController,
                          currentPassword: user.password,
                          passwordConfirmController: passwordConfirmController,
                          passwordNewController: passwordNewController,
                        ),
                        SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ElevatedButton(
                              style: TextButton.styleFrom(
                                  foregroundColor: Colors.white,
                                  backgroundColor: Colors.black),
                              child: Text("Update"),
                              onPressed: () async {
                                try {
                                  isValid = await model.updateUser(
                                      username: usernameController.text
                                          .trim()
                                          .toLowerCase(),
                                      email: emailController.text
                                          .trim()
                                          .toLowerCase(),
                                      passwordConfirm: passwordConfirmController
                                          .text
                                          .trim()
                                          .toLowerCase(),
                                      passwordNew: passwordNewController.text
                                          .trim()
                                          .toLowerCase(),
                                      oldPassword:
                                          user.password.trim().toLowerCase());

                                  if (isValid[0]) {
                                    await model.login(
                                        email: user.email.trim().toLowerCase(),
                                        password:
                                            user.password.trim().toLowerCase());

                                    Navigator.pushNamed(context, "home");
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                  }
                                  if (!isValid[0]) {
                                    dialog.dialogBox(
                                        context, isValid[1], isValid[0]);
                                  }
                                } catch (e) {
                                  dialog.dialogBox(
                                      context, isValid[1], isValid[0]);
                                }
                              },
                            ),
                            ElevatedButton(
                              style: TextButton.styleFrom(
                                  foregroundColor: Colors.white,
                                  backgroundColor: Colors.black),
                              child: Text("Back"),
                              onPressed: () async {
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ))));
  }
}
