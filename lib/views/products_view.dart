// ignore_for_file: prefer_const_constructors, curly_braces_in_flow_control_structures, use_build_context_synchronously, unused_import, unnecessary_import, implementation_imports

import 'dart:convert';
import 'package:stacked/stacked.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart';
import 'package:jobcard/dialog_service/basic_dialog.dart';
import 'package:jobcard/injection/injection.dart';
import 'package:jobcard/model/products_model.dart';
import 'package:jobcard/navigation/app_bar.dart';
import 'package:jobcard/service/products_service.dart';
import 'package:jobcard/ui/products_screen.dart';
import 'package:provider/provider.dart';
import 'package:email_validator/email_validator.dart';

import '../navigation/nav_drawer.dart';
import '../view_model/jobcard_view_model.dart';
import '../view_model/product_view_model.dart';

class ProductsView extends StatefulWidget {
  const ProductsView({super.key});

  @override
  State<ProductsView> createState() => _ProductsViewState();
}

class _ProductsViewState extends State<ProductsView> {
  List productList = [];
  List isValid = [];

  bool exists = false;
  int count = 0;
  final ProductsModel model =
      ProductsModel(productService: getIt<ProductService>());

  final DialogBox dialog = DialogBox();

  final TextEditingController _productNameController = TextEditingController();
  final TextEditingController _productCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ProductsModel>.reactive(
      viewModelBuilder: () =>
          ProductsModel(productService: getIt<ProductService>()),
      onViewModelReady: (model) => model.getProducts(),
      builder: (context, model, child) => Scaffold(
        drawer: NavDrawer(),
        appBar: Custom_AppBar(context, 'Products'),
        backgroundColor: Colors.grey,
        body: ListView.builder(
            itemCount: model.products.length,
            itemBuilder: ((context, index) {
              return ProductsHeader(
                  itemName: model.products[index].productName,
                  itemCode: model.products[index].productCode);
            })),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.black,
          onPressed: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    scrollable: true,
                    title: Text('Register New Product'),
                    content: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Form(
                        autovalidateMode: AutovalidateMode.always,
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                                controller: _productNameController,
                                decoration: InputDecoration(
                                  labelText: 'Product Name',
                                  icon: Icon(Icons.phone_android),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Field must not be empty";
                                  }
                                  return null;
                                }),
                            TextFormField(
                                controller: _productCodeController,
                                decoration: InputDecoration(
                                  labelText: 'Product Code',
                                  icon: Icon(Icons.numbers),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Field must not be empty";
                                  }
                                  return null;
                                }),
                          ],
                        ),
                      ),
                    ),
                    actions: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextButton(
                              child: Text("Back"),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                          TextButton(
                            child: Text("Submit"),
                            onPressed: () async {
                              while (count < model.products.length) {
                                productList
                                    .add(model.products[count].productCode);
                                count++;
                              }

                              if (_productNameController.text.isNotEmpty &&
                                  _productCodeController.text.isNotEmpty &&
                                  !productList
                                      .contains(_productCodeController.text)) {
                                try {
                                  isValid = await model.newProduct(
                                      productName:
                                          _productNameController.text.trim(),
                                      productCode:
                                          _productCodeController.text.trim());

                                  if (isValid[0]) {
                                    dialog.dialogBox(
                                        context,
                                        "New Product has been Registered",
                                        isValid[0]);
                                    _productNameController.clear();
                                    _productCodeController.clear();
                                  } else if (!isValid[0]) {
                                    dialog.dialogBox(
                                        context, isValid[1], isValid[0]);
                                  }
                                } catch (e) {
                                  dialog.dialogBox(
                                      context, isValid[1], isValid[0]);
                                }
                              } else if (productList
                                  .contains(_productCodeController.text)) {
                                dialog.dialogBox(
                                    context, "Product Already Exists", false);
                              } else {
                                dialog.dialogBox(context,
                                    "One or more fields are empty", false);
                              }
                            },
                          ),
                        ],
                      )
                    ],
                  );
                });
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}
