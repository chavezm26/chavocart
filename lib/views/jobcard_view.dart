// ignore_for_file: prefer_const_constructors, use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:jobcard/navigation/app_bar.dart';
import 'package:jobcard/service/jobcard_service.dart';
import 'package:jobcard/ui/jobcard_screen.dart';
import 'package:jobcard/view_model/jobcard_view_model.dart';
import 'package:jobcard/view_model/product_view_model.dart';

import 'package:stacked/stacked.dart';
import '../dialog_service/basic_dialog.dart';
import '../injection/injection.dart';
import '../navigation/nav_drawer.dart';
import '../service/products_service.dart';

class JCView extends StatefulWidget {
  const JCView({super.key});

  @override
  State<JCView> createState() => _JCViewState();
}

class _JCViewState extends State<JCView> {
  final ProductsModel productsModel =
      ProductsModel(productService: getIt<ProductService>());
  List isValid = [];
  final DialogBox dialog = DialogBox();

  final JCModel model = JCModel(jobCardService: getIt<JobCardService>());

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<JCModel>.reactive(
        viewModelBuilder: () =>
            JCModel(jobCardService: getIt<JobCardService>()),
        onViewModelReady: (model) => model.getJC(),
        builder: (context, model, child) => Scaffold(
              drawer: NavDrawer(),
              appBar: Custom_AppBar(context, 'Jobcard'),
              backgroundColor: Colors.grey,
              body: ListView.builder(
                  itemCount: model.cards.length,
                  itemBuilder: ((context, index) {
                    return JobCardHeader(
                      supplierName: model.cards[index].supplierName,
                      customerName: model.cards[index].customerName,
                      purchaseOrder: model.cards[index].purchaseOrder,
                      contactName: model.cards[index].contactName,
                      email: model.cards[index].email,
                      phoneNumber: model.cards[index].phoneNumber,
                      id: model.cards[index].id,
                    );
                  })),
              floatingActionButton: FloatingActionButton(
                backgroundColor: Colors.black,
                onPressed: () {
                  model.clearForm();
                  Navigator.pushNamed(context, 'newJC');
                },
                child: Icon(Icons.add),
              ),
            ));
  }
}
