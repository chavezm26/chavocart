// ignore_for_file: prefer_const_constructors, use_build_context_synchronously, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../dialog_service/basic_dialog.dart';
import '../injection/injection.dart';
import '../model/cartItems_model.dart';
import '../model/jobcard_model.dart';
import '../service/cart_service.dart';
import '../service/jobcard_service.dart';
import '../ui/cart_screen.dart';
import '../view_model/jobcard_view_model.dart';

class CartView extends StatefulWidget {
  CartView({super.key});

  @override
  State<CartView> createState() => _CartViewState();
}

class _CartViewState extends State<CartView> {
  @override
  void dispose() {
    if (products.productCode.isNotEmpty) {
      jcModel.removeItems();
    }

    super.dispose();
  }

  List isValid = [];
  bool visible = false;
  final DialogBox dialog = const DialogBox();
  Cards cards = getIt<JobCardService>().cards;
  final List items = getIt<CartService>().items;
  final List code = getIt<CartService>().code;
  final List quant = getIt<CartService>().quant;
  CartItems products = getIt<JobCardService>().cartItems;
  final CartService cart = CartService();
  final JobCardService jc = JobCardService();
  final JCModel jcModel = JCModel(jobCardService: getIt<JobCardService>());
  @override
  Widget build(BuildContext context) {
    if (products.productName.isEmpty) {
      visible = false;
    } else {
      visible = true;
    }
    return ChangeNotifierProvider(
        create: (context) => CartService(),
        child: Consumer<CartService>(
          builder: (context, model, child) => Scaffold(
            appBar: AppBar(
                automaticallyImplyLeading: true,
                title: Center(child: Text('Shopping Cart')),
                backgroundColor: Colors.black),
            backgroundColor: Colors.grey,
            body: items.isEmpty && products.productCode.isEmpty
                ? const Center(
                    child: Text(
                      "Your cart is empty",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  )
                : products.productCode.isEmpty
                    ? ListView.builder(
                        itemCount: items.length,
                        itemBuilder: ((context, index) {
                          return CartHeader(
                              itemName: items[index].toString(),
                              itemCode: code[index].toString(),
                              quantity: quant[index].toString());
                        }))
                    : ListView.builder(
                        itemCount: items.length,
                        itemBuilder: ((context, index) {
                          return CartHeader(
                              itemName: items[index].toString(),
                              itemCode: code[index].toString(),
                              quantity: quant[index].toString());
                        })),
          ),
        ));
  }
}
