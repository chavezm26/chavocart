import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../injection/injection.dart';
import '../model/user_model.dart';
import 'authentication_service.dart';

final User user = getIt<AuthenticationService>().currentUser;

class Api {
  Uri apiUrl({required String post, Map<String, String>? param}) {
    var url = Uri.http('127.0.0.1:8090', post, param);

    return url;
  }

  Future loginApi({
    required String email,
    required String password,
  }) async {
    var response = await http.post(
        apiUrl(post: '/api/collections/users/auth-with-password'),
        body: {'identity': email, 'password': password});

    return response;
  }

  Future registerApi({
    required String username,
    required String email,
    required String password,
  }) async {
    var response = await http.post(
        apiUrl(post: '/api/collections/users/records'),
        headers: {
          "Content-type": "application/json",
          "Accept": "application/json"
        },
        body: jsonEncode({
          'username': username,
          'email': email,
          'password': password,
          'passwordConfirm': password
        }));
    return response;
  }

  Future updateApi(
      {required String username,
      required String email,
      required String passwordConfirm,
      required String passwordNew,
      required String oldPassword}) async {
    var userID = user.id;
    var response = await http.patch(
        apiUrl(post: '/api/collections/users/records/$userID'),
        headers: {
          "Content-type": "application/json",
          "Accept": "application/json"
        },
        body: jsonEncode({
          'username': username,
          'email': email,
          'passwordConfirm': passwordConfirm,
          'oldPassword': oldPassword,
          'password': passwordNew
        }));
    return response;
  }

  Future fetchApi() async {
    var response = await http.get(
      apiUrl(post: '/api/collections/products/records/'),
    );
    return response;
  }

  Future searchApi(String filter) async {
    var response = await http.get(
      apiUrl(
          post: 'api/collections/products/records',
          param: {'filter': "(productName~'$filter')"}),
    );

    return response;
  }

  Future newProductApi({
    required String productName,
    required String productCode,
  }) async {
    var response = await http.post(
        apiUrl(post: '/api/collections/products/records'),
        headers: {
          "Content-type": "application/json",
          "Accept": "application/json"
        },
        body: jsonEncode({
          'productName': productName,
          'productCode': productCode,
        }));
    return response;
  }

  Future newJobcard({
    required String supplierName,
    required String customerName,
    required String purchaseOrder,
    required String contactName,
    required String email,
    required String phoneNumber,
    required List products,
    required List productCode,
    required List quantity,
    required String jobcardNo,
  }) async {
    var response = await http.post(
        apiUrl(post: '/api/collections/jobcards/records'),
        headers: {
          "Content-type": "application/json",
          "Accept": "application/json"
        },
        body: jsonEncode({
          'supplierName': supplierName,
          'customerName': customerName,
          'purchaseOrder': purchaseOrder,
          'contactName': contactName,
          'email': email,
          'phoneNumber': phoneNumber,
          'products': products,
          'productCode': productCode,
          'quantity': quantity,
          'jobcardNo': jobcardNo,
        }));
    return response;
  }

  Future fetchJC() async {
    var response = await http.get(
      apiUrl(post: '/api/collections/jobcards/records/'),
    );
    return response;
  }

  Future updateJobcard({
    required String id,
    required String supplierName,
    required String customerName,
    required String purchaseOrder,
    required String contactName,
    required String email,
    required String phoneNumber,
    required List products,
    required List productCode,
    required List quantity,
  }) async {
    var response = await http.patch(
        apiUrl(post: '/api/collections/jobcards/records/$id'),
        headers: {
          "Content-type": "application/json",
          "Accept": "application/json"
        },
        body: jsonEncode({
          'supplierName': supplierName,
          'customerName': customerName,
          'purchaseOrder': purchaseOrder,
          'contactName': contactName,
          'email': email,
          'phoneNumber': phoneNumber,
          'products': products,
          'productCode': productCode,
          'quantity': quantity,
        }));
    return response;
  }

  Future updateProducts({
    required String id,
    required List products,
    required List productCode,
    required List quantity,
  }) async {
    var response = await http.patch(
        apiUrl(post: '/api/collections/jobcards/records/$id'),
        headers: {
          "Content-type": "application/json",
          "Accept": "application/json"
        },
        body: jsonEncode({
          'products': products,
          'productCode': productCode,
          'quantity': quantity,
        }));
    return response;
  }

  Future fetchCart(String id) async {
    var response = await http.get(
      apiUrl(post: '/api/collections/jobcards/records/$id'),
    );
    return response;
  }

  Future searchDebtorApi(String filter) async {
    var response = await http.get(
      apiUrl(
          post: 'api/collections/customers/records',
          param: {'filter': "(customerName~'$filter')"}),
    );
    return response;
  }
}
