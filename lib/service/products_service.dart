import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:jobcard/service/api.dart';
import 'package:pocketbase/pocketbase.dart';

import '../model/products_model.dart';

final pb = PocketBase('http://127.0.0.1:8090');
final Api api = Api();

@singleton
class ProductService {
  late Products _products;
  Products get products => _products;

  Future<List<Products>?> fetchProducts() async {
    var response = await api.fetchApi();

    var message = jsonDecode(response.body);
    var items = message["items"];

    if (response.statusCode == 200) {
      List<Products> noteslist = [];

      for (var products in items) {
        noteslist.add(Products.fromJson(products));
      }

      return noteslist;
    }
    return items;
  }

  Future<List<Products>?> searchProducts(String filter) async {
    var response = await api.searchApi(filter);

    var message = jsonDecode(response.body);
    var items = message["items"];

    if (response.statusCode == 200) {
      List<Products> noteslist = [];

      for (var products in items) {
        noteslist.add(Products.fromJson(products));
      }

      return noteslist;
    }
    return items;
  }

  Future newProduct({
    required String productName,
    required String productCode,
  }) async {
    bool result = false;

    var response = await api.newProductApi(
        productName: productName, productCode: productCode);

    var message = jsonDecode(response.body);

    if (response.statusCode == 200) {
      result = true;
      return [result, message["message"]];
    } else {
      result = false;
      return [result, message["message"]];
    }
  }
}
