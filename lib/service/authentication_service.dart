// ignore_for_file: avoid_print

import 'dart:async';
import 'dart:convert';
import 'package:injectable/injectable.dart';
import 'package:jobcard/model/user_model.dart';
import 'package:pocketbase/pocketbase.dart';
import 'api.dart';

final pb = PocketBase('http://127.0.0.1:8090');
final Api api = Api();

@singleton
class AuthenticationService {
  late User _currentUser;
  User get currentUser => _currentUser;

  Future signOut() async {
    try {
      pb.authStore.clear();
    } catch (e) {
      print(e); // TODO: show dialog with error
    }
  }

  Future loginWithEmail({
    required String email,
    required String password,
  }) async {
    bool result = false;

    var response = await api.loginApi(email: email, password: password);

    var message = jsonDecode(response.body);

    if (response.statusCode == 200) {
      var record = message["record"];

      _currentUser = User(
          id: record["id"],
          email: record["email"],
          name: record["username"],
          password: password,
          confirmPassword: password);

      result = true;

      return [result, message["message"]];
    } else {
      result = false;
      return [result, message["message"]];
    }
  }

  Future signUp({
    required String username,
    required String email,
    required String password,
  }) async {
    bool result = false;

    var response = await api.registerApi(
        username: username, email: email, password: password);

    var message = jsonDecode(response.body);

    if (response.statusCode == 200) {
      result = true;
      return [result, message["message"]];
    } else {
      result = false;
      return [result, message["message"]];
    }
  }

  Future updateApi(
      {required String username,
      required String email,
      required String passwordConfirm,
      required String passwordNew,
      required String oldPassword}) async {
    bool result = false;

    var response = await api.updateApi(
        username: username,
        email: email,
        passwordConfirm: passwordConfirm,
        passwordNew: passwordNew,
        oldPassword: oldPassword);
    print(response.body);
    print(response.statusCode);
    var message = jsonDecode(response.body);
    if (response.statusCode == 200) {
      _currentUser = User(
          email: email,
          name: username,
          password: passwordNew,
          confirmPassword: passwordConfirm);
      result = true;
      return [result, message["message"]];
    } else {
      result = false;
      return [result, message["message"]];
    }
  }
}
