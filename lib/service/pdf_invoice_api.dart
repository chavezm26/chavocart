import 'package:intl/intl.dart';

import 'package:flutter/services.dart';
import 'package:jobcard/service/pdf_api.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';

class PdfInvoiceApi {
  generate(
    String customerName,
    String contactName,
    String email,
    String phoneNumber,
    String jobcardNo,
    List<List> data,
  ) async {
    final pdf = Document();
    final date = DateTime.now();
    String dateNow = DateFormat("yyyy-MM-dd HH:mm").format(date);
    final font = await rootBundle.load("assets/OpenSans-Regular.ttf");
    final ttf = Font.ttf(font);
    final headers = [
      "Product Code ",
      "Product Name",
      "Quantity",
    ];
    pdf.addPage(
      MultiPage(
        pageFormat: PdfPageFormat.a4,
        margin: const EdgeInsets.all(32),
        build: (Context context) {
          return [
            Header(
              margin: const EdgeInsets.all(30),
              level: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                          'Debtor: $customerName \nContact: $contactName\nEmail Address: $email\nContact No: $phoneNumber',
                          style: TextStyle(font: ttf, fontSize: 10)),
                      Container(
                          height: 50,
                          width: 50,
                          child: BarcodeWidget(
                              barcode: Barcode.qrCode(), data: jobcardNo)),
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(dateNow, style: TextStyle(font: ttf, fontSize: 10)),
                      Text("Jobcard No: $jobcardNo",
                          style: TextStyle(fontSize: 12, fontBold: ttf)),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Center(
                child: TableHelper.fromTextArray(
                    headers: headers,
                    data: data,
                    headerAlignment: Alignment.topLeft)),
          ];
        },
      ),
    );

    return PdfApi.saveDocument(jobcard: jobcardNo, pdf: pdf);
  }

  Future savePdf() async {}
}
