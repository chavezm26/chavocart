import 'dart:io';

import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/widgets.dart';

class PdfApi {
  static Future<File> saveDocument({
    required String jobcard,
    required Document pdf,
  }) async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();

    String documentsPath = documentDirectory.path;

    File file = File("$documentsPath/Jobcard_$jobcard.pdf");
    final byte = await pdf.save();

    file.writeAsBytesSync(byte);

    return file;
  }

  static Future openFile(File file) async {
    final url = file.path;

    await OpenFile.open(url);
  }
}
