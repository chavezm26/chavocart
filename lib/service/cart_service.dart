// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

List _items = [];
List _code = [];
List _quant = [];

@singleton
class CartService extends ChangeNotifier {
  bool addToCart(String itemName, String itemCode, String quantity) {
    if (_items.contains(itemName)) {
      _quant.removeAt(_items.indexOf(itemName));
      _quant.insert(_items.indexOf(itemName), quantity);
      return false;
    } else {
      _items.add(itemName);
      _code.add(itemCode);
      _quant.add(quantity);
      notifyListeners();
      return true;
    }
  }

  void removeFromCart(String itemName, String itemCode, String quantity) {
    _items.remove(itemName);
    _code.remove(itemCode);
    _quant.remove(quantity);
    notifyListeners();
  }

  void removeAllFromCart() {
    _items = [];
    _code = [];
    _quant = [];
    notifyListeners();
  }

  get items => _items;
  get code => _code;
  get quant => _quant;
}
