import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:jobcard/model/cartItems_model.dart';
import 'package:jobcard/model/jobcard_model.dart';
import 'package:jobcard/service/cart_service.dart';
import 'package:pocketbase/pocketbase.dart';

import '../model/debtor_model.dart';
import 'api.dart';

final pb = PocketBase('http://127.0.0.1:8090');
final Api api = Api();

@singleton
class JobCardService extends ChangeNotifier {
  Cards _cards = Cards(products: [], quantity: []);
  Cards get cards => _cards;

  CartItems _cartItems =
      CartItems(productName: [], productCode: [], quantity: []);
  CartItems get cartItems => _cartItems;

  Debtor _debtor = Debtor(customerName: "", debtorCode: "");
  Debtor get debtor => _debtor;
  CartService cart = CartService();

  Future newJC(
      {required String supplierName,
      required String customerName,
      required String purchaseOrder,
      required String contactName,
      required String email,
      required String phoneNumber,
      required List products,
      required List productCode,
      required List quantity,
      required String jobcardNo}) async {
    bool result = false;

    var response = await api.newJobcard(
        supplierName: supplierName,
        purchaseOrder: purchaseOrder,
        contactName: contactName,
        email: email,
        phoneNumber: phoneNumber,
        products: products,
        productCode: productCode,
        quantity: quantity,
        jobcardNo: jobcardNo,
        customerName: customerName);

    var message = jsonDecode(response.body);

    if (response.statusCode == 200) {
      result = true;
      return [result, message["message"]];
    } else {
      result = false;
      return [result, message["message"]];
    }
  }

  Future<List<Cards>?> fetchJC() async {
    var response = await api.fetchJC();

    var message = jsonDecode(response.body);
    var items = message["items"];

    if (response.statusCode == 200) {
      List<Cards> noteslist = [];

      for (var products in items) {
        noteslist.add(Cards.fromJson(products));
      }

      return noteslist;
    }
    return items;
  }

  Future updateJC({
    required String id,
    required String supplierName,
    required String customerName,
    required String purchaseOrder,
    required String contactName,
    required String email,
    required String phoneNumber,
    required List products,
    required List productCode,
    required List quantity,
  }) async {
    bool result = false;

    var response = await api.updateJobcard(
      id: id,
      supplierName: supplierName,
      purchaseOrder: purchaseOrder,
      contactName: contactName,
      email: email,
      phoneNumber: phoneNumber,
      products: products,
      productCode: productCode,
      quantity: quantity,
      customerName: customerName,
    );

    var message = jsonDecode(response.body);

    if (response.statusCode == 200) {
      result = true;
      return [result, message["message"]];
    } else {
      result = false;
      return [result, message["message"]];
    }
  }

  Future updateProducts({
    required String id,
    required List products,
    required List productCode,
    required List quantity,
  }) async {
    bool result = false;

    var response = await api.updateProducts(
      id: id,
      products: products,
      productCode: productCode,
      quantity: quantity,
    );

    var message = jsonDecode(response.body);

    if (response.statusCode == 200) {
      result = true;
      return [result, message["message"]];
    } else {
      result = false;
      return [result, message["message"]];
    }
  }

  Future itemsInCart({required String id}) async {
    var response = await api.fetchCart(id);
    var message = jsonDecode(response.body);
    _cartItems = CartItems(
        productName: message["products"],
        productCode: message["productCode"],
        quantity: message["quantity"]);

    _cards = Cards(
        id: id,
        supplierName: message["supplierName"],
        customerName: message["customerName"],
        purchaseOrder: message["purchaseOrder"],
        contactName: message["contactName"],
        email: message["email"],
        phoneNumber: message["phoneNumber"],
        products: message["products"],
        quantity: message["quantity"],
        jobcardNo: message["jobcardNo"]);

    _debtor = Debtor(
      customerName: message["customerName"],
    );

    var cartCount = 0;
    while (cartCount < message["products"].length) {
      cart.addToCart(message["products"][cartCount],
          message["productCode"][cartCount], message["quantity"][cartCount]);
      cartCount++;
    }

    notifyListeners();
  }

  void clearForm() {
    _cards = Cards(
        id: "",
        supplierName: "",
        purchaseOrder: "",
        contactName: "",
        email: "",
        phoneNumber: "",
        products: [],
        quantity: [],
        jobcardNo: "");

    notifyListeners();
  }

  void removeItems() {
    _cartItems = CartItems(productName: [], productCode: [], quantity: []);

    notifyListeners();
  }
}
