import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:jobcard/service/api.dart';
import 'package:pocketbase/pocketbase.dart';

import '../model/debtor_model.dart';

final pb = PocketBase('http://127.0.0.1:8090');
final Api api = Api();

@singleton
class DebtorService {
  late Debtor _debtor;
  Debtor get debtor => _debtor;

  Future<List<Debtor>?> searchDebtor(String filter) async {
    var response = await api.searchDebtorApi(filter);

    var message = jsonDecode(response.body);
    var items = message["items"];

    if (response.statusCode == 200) {
      List<Debtor> noteslist = [];

      for (var customerName in items) {
        noteslist.add(Debtor.fromJson(customerName));
      }

      return noteslist;
    }
    return items;
  }
}
