// ignore_for_file: prefer_const_constructors, prefer_const_constructors_in_immutables, body_might_complete_normally_nullable, must_be_immutable

import 'package:flutter/material.dart';

import 'package:jobcard/service/cart_service.dart';

import '../dialog_service/basic_dialog.dart';

class ProductsHeader extends StatelessWidget {
  final String itemName;
  final String itemCode;

  ProductsHeader({
    super.key,
    required this.itemName,
    required this.itemCode,
  });
  final CartService model = CartService();
  final DialogBox dialog = DialogBox();
  bool result = false;
  @override
  Widget build(BuildContext context) {
    final TextEditingController quantityController = TextEditingController();
    return Padding(
        padding: const EdgeInsets.all(8),
        child: Card(
          color: Colors.black12,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Product Name: $itemName',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 8, left: 8),
                  child: Text(
                    'Product Code: $itemCode',
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 8, left: 5),
                    child: ElevatedButton(
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: Text("Please input the Quanity"),
                                content: Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: Form(
                                    autovalidateMode: AutovalidateMode.always,
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        TextFormField(
                                            controller: quantityController,
                                            decoration: InputDecoration(
                                              labelText: 'Quantity',
                                              icon: Icon(Icons.numbers),
                                            ),
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return "Field must not be empty";
                                              } else if (int.tryParse(value) ==
                                                  null) {
                                                return 'Only Number are allowed';
                                              }
                                              return null;
                                            }),
                                      ],
                                    ),
                                  ),
                                ),
                                actions: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      TextButton(
                                          onPressed: () {
                                            try {
                                              result = model.addToCart(
                                                itemName,
                                                itemCode,
                                                quantityController.text,
                                              );
                                              result == true
                                                  ? dialog.dialogBox(
                                                      context,
                                                      "Item has been added to cart",
                                                      true)
                                                  : dialog.dialogBox(
                                                      context,
                                                      "Item is already in the cart",
                                                      false);
                                            } catch (e) {
                                              debugPrint;
                                            }
                                          },
                                          child: const Text("Add to cart")),
                                      TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: const Text("Close")),
                                    ],
                                  ),
                                ],
                              );
                            });
                      },
                      style: TextButton.styleFrom(
                          foregroundColor: Colors.white,
                          backgroundColor: Colors.black),
                      child: Text("Add to cart"),
                    )),
              ]),
        ));
  }
}
