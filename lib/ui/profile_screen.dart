import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class ProfileHeader extends StatelessWidget {
  final TextEditingController emailController;
  final String currentPassword;
  final TextEditingController usernameController;
  final TextEditingController passwordConfirmController;
  final TextEditingController passwordNewController;

  const ProfileHeader({
    super.key,
    required this.emailController,
    required this.usernameController,
    required this.currentPassword,
    required this.passwordConfirmController,
    required this.passwordNewController,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(children: <Widget>[
        const SizedBox(height: 20),
        const Text(
          'Update details below',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        UsernameTextField(usernameController),
        LoginTextField(emailController),
        Text(
          "Current Password: $currentPassword",
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        PasswordNewTextField(passwordNewController),
        PasswordConfirmTextField(passwordConfirmController),
      ]),
    );
  }
}

class LoginTextField extends StatelessWidget {
  final TextEditingController controller;

  const LoginTextField(this.controller, {super.key});

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidateMode: AutovalidateMode.always,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: TextFormField(
          decoration: const InputDecoration(
              label: Text('Email Address'),
              floatingLabelBehavior: FloatingLabelBehavior.auto),
          controller: controller,
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "Field must not be empty";
            } else if (!EmailValidator.validate(value)) {
              return "Email address is invalid";
            }
          },
        ),
      ),
    );
  }
}

class UsernameTextField extends StatelessWidget {
  final TextEditingController usernameController;

  const UsernameTextField(this.usernameController, {super.key});

  @override
  Widget build(BuildContext context) {
    return Form(
        autovalidateMode: AutovalidateMode.always,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: TextFormField(
              decoration: const InputDecoration(
                  label: Text('Username'),
                  floatingLabelBehavior: FloatingLabelBehavior.auto),
              controller: usernameController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "Field must not be empty";
                }
                return null;
              }),
        ));
  }
}

class PasswordConfirmTextField extends StatelessWidget {
  final TextEditingController passwordConfirmController;

  const PasswordConfirmTextField(this.passwordConfirmController, {super.key});

  @override
  Widget build(BuildContext context) {
    return Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: TextFormField(
              decoration: const InputDecoration(
                  label: Text('Confirm Password'),
                  floatingLabelBehavior: FloatingLabelBehavior.auto),
              controller: passwordConfirmController,
              validator: (value) {
                if (value != null && value.length < 5) {
                  return "Password must be min. 5 Characters";
                }

                return null;
              }),
        ));
  }
}

class PasswordNewTextField extends StatelessWidget {
  final TextEditingController passwordNewController;

  const PasswordNewTextField(this.passwordNewController, {super.key});

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: TextFormField(
              decoration: const InputDecoration(
                  label: Text('Password'),
                  floatingLabelBehavior: FloatingLabelBehavior.auto),
              controller: passwordNewController,
              validator: (value) {
                if (value != null && value.length < 5) {
                  return "Password must be min. 5 Characters";
                }

                return null;
              })),
    );
  }
}
