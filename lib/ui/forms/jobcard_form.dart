// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class JCForm extends StatelessWidget {
  TextEditingController controller;
  String label;
  Icon icon;
  JCForm(
      {super.key,
      required this.controller,
      required this.label,
      required this.icon});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        controller: controller,
        decoration: InputDecoration(
          labelText: label,
          icon: icon,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "Field must not be empty";
          }
          return null;
        });
  }
}
