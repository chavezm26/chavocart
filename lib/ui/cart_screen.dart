// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:jobcard/service/cart_service.dart';

class CartHeader extends StatelessWidget {
  final String itemName;
  final String itemCode;
  String quantity;
  CartHeader(
      {super.key,
      required this.itemName,
      required this.itemCode,
      required this.quantity});

  final CartService cart = CartService();

  @override
  Widget build(BuildContext context) {
    final TextEditingController controller =
        TextEditingController(text: quantity);
    return Padding(
        padding: const EdgeInsets.all(8),
        child: Card(
            color: Colors.black12,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Product Name: $itemName',
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 8, left: 8),
                    child: Text(
                      'Product Code: $itemCode',
                      style: const TextStyle(
                          fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 8, left: 8),
                    child: Text(
                      'Quantity:',
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Form(
                    autovalidateMode: AutovalidateMode.always,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: SizedBox(
                        width: 250,
                        height: 40,
                        child: TextFormField(
                          style: const TextStyle(color: Colors.white),
                          decoration: const InputDecoration(
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            fillColor: Colors.black,
                            filled: true,
                          ),
                          controller: controller,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Field must not be empty";
                            } else if (int.tryParse(value) == null) {
                              return 'Only Number are allowed';
                            }
                          },
                          onChanged: (text) {
                            quantity = controller.text;
                            cart.addToCart(itemName, itemCode, quantity);
                          },
                        ),
                      ),
                    ),
                  ),
                  Padding(
                      padding:
                          const EdgeInsets.only(top: 5, bottom: 8, left: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            icon: const Icon(
                              Icons.delete_outline_sharp,
                              color: Colors.black,
                            ),
                            onPressed: () async {
                              cart.removeFromCart(itemName, itemCode, quantity);
                            },
                          ),
                        ],
                      ))
                ])));
  }
}
