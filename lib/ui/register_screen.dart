// ignore_for_file: prefer_const_constructors_in_immutables, prefer_const_constructors, unnecessary_import

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class RegisterHeader extends StatelessWidget {
  final TextEditingController emailController;
  final TextEditingController passwordController;
  final TextEditingController usernameController;

  const RegisterHeader({
    super.key,
    required this.emailController,
    required this.passwordController,
    required this.usernameController,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(children: <Widget>[
        SizedBox(height: 20),
        Text(
          'Please complete form below',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        UsernameTextField(usernameController),
        LoginTextField(emailController),
        PasswordTextField(passwordController),
      ]),
    );
  }
}

class LoginTextField extends StatelessWidget {
  final TextEditingController controller;

  LoginTextField(this.controller, {super.key});

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: TextFormField(
          decoration: const InputDecoration(
              label: Text('Email Address'),
              floatingLabelBehavior: FloatingLabelBehavior.auto),
          controller: controller,
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "Field must not be empty";
            } else if (!EmailValidator.validate(value)) {
              return "Email address is invalid";
            }
          },
        ),
      ),
    );
  }
}

class PasswordTextField extends StatelessWidget {
  final TextEditingController passwordController;

  PasswordTextField(this.passwordController, {super.key});

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: TextFormField(
            decoration: const InputDecoration(
                label: Text('Password'),
                floatingLabelBehavior: FloatingLabelBehavior.auto),
            controller: passwordController,
            obscureText: true,
            validator: (value) {
              if (value != null && value.length < 5) {
                return "Password must be min. 5 Characters";
              }

              return null;
            }),
      ),
    );
  }
}

class UsernameTextField extends StatelessWidget {
  final TextEditingController usernameController;

  UsernameTextField(this.usernameController, {super.key});

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidateMode: AutovalidateMode.always,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: TextFormField(
            decoration: const InputDecoration(
                label: Text('Username'),
                floatingLabelBehavior: FloatingLabelBehavior.auto),
            controller: usernameController,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "Field must not be empty";
              }
              return null;
            }),
      ),
    );
  }
}
