// ignore_for_file: must_be_immutable, use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:jobcard/model/cart_model.dart';
import 'package:jobcard/model/jobcard_model.dart';
import 'package:jobcard/service/jobcard_service.dart';

import '../injection/injection.dart';
import '../model/cartItems_model.dart';
import '../service/cart_service.dart';
import '../service/pdf_api.dart';
import '../service/pdf_invoice_api.dart';
import '../view_model/jobcard_view_model.dart';

class JobCardHeader extends StatelessWidget {
  final String supplierName;
  final String customerName;
  final String purchaseOrder;
  final String contactName;
  final String email;
  final String phoneNumber;
  final String id;

  JobCardHeader(
      {super.key,
      required this.supplierName,
      required this.customerName,
      required this.purchaseOrder,
      required this.contactName,
      required this.email,
      required this.phoneNumber,
      required this.id});
  Cart cartModel = Cart();
  final PdfInvoiceApi _pdfInvoiceApi = PdfInvoiceApi();
  final CartService cart = CartService();
  final JCModel jcModel = JCModel(jobCardService: getIt<JobCardService>());
  final snackBar = const SnackBar(content: Text("PDF has been created"));

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Card(
        color: Colors.black12,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Supplier Name: $supplierName',
                style:
                    const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Customer Name: $customerName',
                style:
                    const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 8, left: 8),
              child: Text(
                'Purchase Order: $purchaseOrder',
                style:
                    const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 8, left: 8),
              child: Text(
                'Contact Name: $contactName',
                style:
                    const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 8, left: 8),
              child: Text(
                'Email Address: $email',
                style:
                    const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 8, left: 8),
              child: Text(
                'Phone Number: $phoneNumber',
                style:
                    const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
                padding:
                    const EdgeInsets.only(top: 5, bottom: 8, left: 5, right: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: const Icon(
                        Icons.edit,
                        color: Colors.black,
                      ),
                      onPressed: () async {
                        cart.removeAllFromCart();
                        await jcModel.itemsInCart(id: id);

                        Navigator.pushNamed(context, 'newJC');
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        ElevatedButton(
                            style: TextButton.styleFrom(
                                foregroundColor: Colors.white,
                                backgroundColor: Colors.black),
                            onPressed: () async {
                              await jcModel.itemsInCart(id: id);
                              Cards cards = getIt<JobCardService>().cards;
                              CartItems products =
                                  getIt<JobCardService>().cartItems;

                              List<List> data = [];

                              var index = 0;
                              while (index < products.productName.length) {
                                data.add([
                                  products.productCode[index],
                                  products.productName[index],
                                  products.quantity[index]
                                ]);

                                index++;
                              }

                              final pdfFile = await _pdfInvoiceApi.generate(
                                  customerName,
                                  contactName,
                                  email,
                                  phoneNumber,
                                  cards.jobcardNo,
                                  data);

                              PdfApi.openFile(pdfFile);
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                            },
                            child: const Text("Export to PDF")),
                      ],
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
