// ignore_for_file: constant_identifier_names

class RoutePaths {
  static const String Login = 'login';
  static const String Nav = '/';
  static const String Profile = 'profile';
  static const String Register = 'register';
  static const String Home = 'home';
  static const String Products = 'products';
  static const String Jobcard = 'jobcard';
  static const String Cart = 'cart';
  static const String NewJC = 'newJC';
}
