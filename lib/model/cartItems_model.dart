// ignore_for_file: empty_constructor_bodies, file_names

class CartItems {
  List productName;
  List productCode;
  List quantity;

  CartItems({
    required this.productName,
    required this.productCode,
    required this.quantity,
  });

  CartItems.fromJson(Map<String, dynamic> json)
      : productName = json['productName'],
        productCode = json['productCode'],
        quantity = json['quantity'];

  Map<String, dynamic> toJson() => {
        "productName": productName,
        "productCode": productCode,
        "quantity": quantity
      };
}
