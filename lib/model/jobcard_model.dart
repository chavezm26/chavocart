class Cards {
  final String id;
  final String supplierName;
  final String customerName;
  final String purchaseOrder;
  final String contactName;
  final String email;
  final String phoneNumber;
  final List products;
  final List quantity;
  final String jobcardNo;

  Cards({
    this.id = "",
    this.supplierName = "",
    this.purchaseOrder = "",
    this.contactName = "",
    this.email = "",
    this.phoneNumber = "",
    required this.products,
    required this.quantity,
    this.jobcardNo = "",
    this.customerName = "",
  });

  Cards.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        supplierName = json['supplierName'],
        customerName = json['customerName'],
        purchaseOrder = json['purchaseOrder'],
        contactName = json['contactName'],
        email = json['email'],
        phoneNumber = json['phoneNumber'],
        products = json['products'],
        quantity = json['quantity'],
        jobcardNo = json['jobcardNo'];

  Map<String, dynamic> toJson() => {
        "id": id,
        "supplierName": supplierName,
        "customerName": customerName,
        "purchaseOrder": purchaseOrder,
        "contactName": contactName,
        "email": email,
        "phoneNumber": phoneNumber,
        "products": products,
        "quantity": quantity,
        "jobcardNo": jobcardNo
      };
  static List<Cards> fromJsonList(List list) {
    return list.map((item) => Cards.fromJson(item)).toList();
  }

  ///this method will prevent the override of toString
  String userAsString() {
    return '$supplierName  #$purchaseOrder ';
  }

  @override
  String toString() => supplierName;
}
