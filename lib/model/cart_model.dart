// ignore_for_file: empty_constructor_bodies, unused_import

import 'dart:convert';

import 'package:jobcard/views/profile_view.dart';

class Cart {
  String productName;
  String productCode;
  String quantity;

  Cart({
    this.productName = "",
    this.productCode = "",
    this.quantity = "",
  });

  Cart.fromJson(Map<String, dynamic> json)
      : productName = json['productName'],
        productCode = json['productCode'],
        quantity = json['quantity'];

  Map<String, dynamic> toJson() => {
        "productName": productName,
        "productCode": productCode,
        "quantity": quantity
      };
}
