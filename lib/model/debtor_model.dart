// ignore_for_file: empty_constructor_bodies, unused_import

import 'dart:convert';

import 'package:jobcard/views/profile_view.dart';

class Debtor {
  String customerName;
  String debtorCode;

  Debtor({
    this.customerName = "",
    this.debtorCode = "",
  });

  Debtor.fromJson(Map<String, dynamic> json)
      : customerName = json['customerName'],
        debtorCode = json['debtorCode'];

  Map<String, dynamic> toJson() =>
      {"customerName": customerName, "debtorCode": debtorCode};

  static List<Debtor> fromJsonList(List list) {
    return list.map((item) => Debtor.fromJson(item)).toList();
  }

  ///this method will prevent the override of toString
  String userAsString() {
    return '#$customerName';
  }

  String codeAsString() {
    return '#$debtorCode';
  }

  @override
  String toString() => customerName;
}
