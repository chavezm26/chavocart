// ignore_for_file: empty_constructor_bodies, unused_import

import 'dart:convert';

import 'package:jobcard/views/profile_view.dart';
import 'package:pocketbase/pocketbase.dart';

final pb = PocketBase('http://127.0.0.1:8090');

class User {
  final String id;
  final String email;
  final String name;
  final String password;
  final String confirmPassword;

  User(
      {this.id = "",
      this.email = "",
      this.name = "",
      this.password = "",
      this.confirmPassword = ""});
  //This id,email,name comes from the auth service

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        name = json['name'],
        password = json['password'],
        confirmPassword = json['confirmPassword'];

  Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "name": name,
        "password": password,
        "confirmPassword": confirmPassword
      };
}
