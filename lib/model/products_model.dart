// ignore_for_file: empty_constructor_bodies, unused_import

import 'dart:convert';

import 'package:jobcard/views/profile_view.dart';

class Products {
  String id;
  String productName;
  String productCode;

  Products({
    this.id = "",
    this.productName = "",
    this.productCode = "",
  });

  Products.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        productName = json['productName'],
        productCode = json['productCode'];

  Map<String, dynamic> toJson() =>
      {"id": id, "productName": productName, "productCode": productCode};

  static List<Products> fromJsonList(List list) {
    return list.map((item) => Products.fromJson(item)).toList();
  }

  ///this method will prevent the override of toString
  String userAsString() {
    return '#$productName';
  }

  String codeAsString() {
    return '#$productCode';
  }

  @override
  String toString() => productName;
}
